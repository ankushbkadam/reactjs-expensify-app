import { firebase, googleAuthProvider } from '../firebase/firebase';

//SIGN_IN
export const startLogin = () => {
    return () => {
        return firebase.auth().signInWithPopup(googleAuthProvider);
    };
};

export const login = (uid) => ({
    type: 'LOGIN',
    uid
}); 

//SIGN_OUT
export const startLogout = () => {
    return () => {
        return firebase.auth().signOut();
    };
};

export const logout = () => ({
    type: 'LOGOUT'
});