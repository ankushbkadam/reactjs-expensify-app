// import uuid from 'uuid';//no need anymore: firebase doing its work of generating unique id
import database from '../firebase/firebase';

//ADD_EXPENSE (similar thing done: for referense: redux-101.js incrementCount.js)
export const addExpense = (expense) => ({
    type: 'ADD_EXPENSE',
    expense
});

export const startAddExpense = (expenseData = {}) => {
    //we cannot return a function
    //so to allow that we have installed redux-thunk dependency.
    //and configured redux-thunk in configureStore.js
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        const {
            description = '',
            note = '',
            amount = 0,
            createdAt = 0
        } = expenseData;

        const expense = { description, note, amount, createdAt };
        //add in firebase and after adding, add in redux store in then()
        database.ref(`users/${uid}/expenses`)
            .push(expense)              //add in firebase
            .then((ref) => {
                dispatch(addExpense({   //add in redux store
                    id: ref.key,//after adding in firebase we get ref obj in return we can access it to set our id.
                    ...expense
                }));
            });

    };
};


//REMOVE_EXPENSE 
export const removeExpense = ({ id } = {}) => ({
    type: 'REMOVE_EXPENSE',
    id
});

export const startRemoveExpense = ({ id } = {}) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses/${id}`).remove().then(() => {
            dispatch(removeExpense({ id }));
        });
    };
};

//EDIT_EXPENSE
export const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
});

export const startEditExpense = (id, updates) => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses/${id}`).update(updates).then(() => {
            dispatch(editExpense(id, updates));
        });
    };
};


//SET_EXPENSES
export const setExpenses = (expenses) => ({
    type: 'SET_EXPENSES',
    expenses
});

export const startSetExpenses = () => {
    return (dispatch, getState) => {
        const uid = getState().auth.uid;
        return database.ref(`users/${uid}/expenses`).once('value').then((snapshot) => {
            const expenses = [];

            snapshot.forEach((childSnapshot) => {
                expenses.push({
                    id: childSnapshot.key,
                    ...childSnapshot.val()
                });
            });
            
            //save expenses in redux-store
            dispatch(setExpenses(expenses));
        });
    };
};