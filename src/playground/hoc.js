/** HOC: Higher Order Components: ref: vid-100
*   defination: A component(hoc) that renders another component.
*   Advantages:
*       -Reuse code.
*       -Render hijacking
*       -Prop manipulation
*       -Abstract state
*/

import React from 'react';
import ReactDOM from 'react-dom';

const Info = (props) => (
    <div>
        <h1>Info</h1>
        <p>The information is: {props.info}</p>
    </div>
);

const withAdminWarnning = (WrappedComponent) => {
    return (props) => (
        <div>
            { props.isAdmin && <p>This is private info. Dont share this with anyone.</p> }
            <WrappedComponent { ...props }/>  {/* here spread operator(...props) get all props provided and pass it as it is */}
        </div>
    );
};

//withAdminWarnning is method, Info is component, withAdminWarnning returns a HOC,
//we can pass any component to get HOC with that component.
const AdminInfo = withAdminWarnning(Info);

//ReactDOM.render(<AdminInfo isAdmin={true} info="aE5n3sdeLKIfn0$."/>, document.getElementById('app'));


//challange:
const requireAuthentication = (WrappedComponent) => {
    return (props) => (
        <div>
            { props.isAuthenticated ? (
                    <WrappedComponent { ...props } />
                ) : (
                    <p>User is not authenticated!</p>
                ) 
            }
        </div>
    );
};
const AuthInfo = requireAuthentication(Info);

ReactDOM.render(<AuthInfo isAuthenticated={false} info="aE5n3sdeLKIfn0$."/>, document.getElementById('app'));
