/**
 * Object destructuring
 */
console.log('This is object destructuring\n');

// const person = {
//     name: 'MyName',
//     age: 25,
//     location: {
//         city: 'Pune',
//         temp: 30
//     }
// };

// //const { name, age } = person; //normal
// //or
// //const { name: firstName, age } = person; //rename it
// //or
// //const { name = 'Unknown', age } = person; //specify default value, value used when not found in obj
// //or
// const { name: firstName = 'Unknown', age } = person;//rename and default value together
// const { city, temp } = person.location;
// console.log(`name: ${firstName}, age: ${age}, city: ${city}, temp: ${temp}`);


const book = {
    title: 'MyTitle',
    author: 'BookAuthor',
    publisher: {
        name: 'PublishfkjgdkljfkjferName'
    }
};
const { name: publisherName } = book.publisher;
console.log(publisherName);



/**
 * Array destructuring
 */
console.log('\n\nThis is Array destructuring\n');

const address = ['A-501 ABC Society', 'Pune', 'Maharashtra', '412345'];
const [ society, city, state, pincode ] = address;
//or
//const [ , city ] = address;
//const [ , city, , pincode] = address;
//const [ , , , pincode] = address;//not same as const [ pincode ] = address
//const [ society, , , ] = address;//same as const [ society ] = address
console.log(`${city} ${pincode}`);

