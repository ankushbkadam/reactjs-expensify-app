import { createStore, combineReducers } from 'redux';
import uuid from 'uuid';

//ADD_EXPENSE (similar thing done: for referense: redux-101.js incrementCount)
const addExpense = (
    {  
        description = '',
        note = '',
        amount = 0,
        createdAt = 0
    } = {}
) => ({
    type: 'ADD_EXPENSE',
    expense: {
        id: uuid(),
        description,
        note,
        amount,
        createdAt
    }
});

//REMOVE_EXPENSE 
const removeExpense = ({ id } = {}) => ({
    type: 'REMOVE_EXPENSE',
    id
});

//EDIT_EXPENSE
const editExpense = (id, updates) => ({
    type: 'EDIT_EXPENSE',
    id,
    updates
});

//SET_TEXT_FILTER
const setTextFilter = (text = '') => ({
    type: 'SET_TEXT_FILTER',
    text
});

//SORT_BY_AMOUNT
const sortByAmount = () => ({ 
    type: 'SORT_BY_AMOUNT'
});

//SORT_BY_DATE
const sortByDate = () => ({
    type: 'SORT_BY_DATE'
});

//SET_START_DATE
const setStartDate = (startDate) => ({
    type: 'SET_START_DATE',
    startDate
});

//SET_END_DATE
const setEndDate = (endDate) => ({
    type: 'SET_END_DATE',
    endDate
});


//Expense reducer:
const expensesReducerDefaultState = [];
const expensesReducer = (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            //return state.concat(action.expenses);
            //or : both(above,below) are exactly same and working
            return [
                ...state,
                action.expense
            ];
        case 'REMOVE_EXPENSE':
            return state.filter(({ id }) => id !== action.id );
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                if (expense.id === action.id) { //if id matches: update
                    return {
                        ...expense,             //get old values (called object spread operator)
                        ...action.updates       //override with new values
                    };
                } else {                        //if id not matches: return same data
                    return expense;
                };
            });
        default: 
            return state;
    }
};

//Filter reducer: 
const filtersReducerDefaultState = {
    text: '',
    sortBy: 'date',//'amount' or date
    startDate: undefined,
    endDate: undefined
};
const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch(action.type) {
        case 'SET_TEXT_FILTER':
            return {
                ...state,           //get old value
                text: action.text   //override old with new
            };
        case 'SORT_BY_AMOUNT':
            return {
                ...state,
                sortBy: 'amount'
            };
        case 'SORT_BY_DATE':
            return {
                ...state,
                sortBy: 'date'
            };
        case 'SET_START_DATE':
            return {
                ...state,
                startDate: action.startDate
            };
        case 'SET_END_DATE':
            return {
                ...state,
                endDate: action.endDate
            };
        default: 
            return state;
    }
};

//get visible expenses
const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate}) => {
    return expenses.filter((expense) => {
        const startDateMatch = typeof startDate !== 'number' || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== 'number' || expense.createdAt <= endDate;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());

        return startDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if(sortBy === 'date') {
            return a.createdAt < b.createdAt ? 1 : -1;
        } else if (sortBy === 'amount') {
            return a.amount < b.amount ? 1 : -1; //1 means b comes first in list -1 means a comes first in list
        }
    });
};


//Store creation:
const store = createStore(
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
);

store.subscribe(() => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);

    console.log(visibleExpenses);
});


//calls
const expenseOne = store.dispatch(addExpense({ description:'rent', amount: 300, createdAt: -19 }));
const expenseTwo = store.dispatch(addExpense({ description:'cloths', amount: 500, createdAt: 203453 }));

// store.dispatch(removeExpense({ id: expenseOne.expense.id }));
// store.dispatch(editExpense(expenseTwo.expense.id, { amount: 34000 } ));

// store.dispatch(setTextFilter('cloths'));
// store.dispatch(setTextFilter());

// store.dispatch(sortByAmount()); //big amount first
store.dispatch(sortByDate()); //big date first: means: recent expenses first

// store.dispatch(setStartDate(0));
// store.dispatch(setStartDate());
// store.dispatch(setEndDate(-1000));


const demoState = {
    expenses: [{
        id: '123',
        description: 'rent',
        note: 'June 2019 rent',
        amount: 25000,
        createdAt: 0
    }],
    filters: {
        text: 'rent',
        sortBy: 'amount',//'amount' or date
        startDate: undefined,
        endDate: undefined
    }
};