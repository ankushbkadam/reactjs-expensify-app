/**
 * New code: action object destructuring used
 */

import { createStore } from 'redux';

const incrementCount = ({ incrementBy = 1 } = {}) => ({
    type: 'INCREMENT',
    incrementBy
});
/** Description for incrementCount:
 *  if object is passed from function call then object will be used,
 *  if object is not passed then default object {} is used,
 * 
 *  from the object (which can be from either of above stmt) 
 *  take incrementBy property as destructuting and set its default value to 1
 *  (reference for destructuring: destructuring.js)
 * 
 *  inside arrow function set this property to incrementBy (as name is same written in short)
*/

const decrementCount = ({ decrementBy = 1 } = {}) => ({
    type: 'DECREMENT',
    decrementBy
});

const resetCount = () => ({
    type: 'RESET'
});

const setCount = ({ count }) => ({//here we forced to pass object with count else err
    type: 'SET',
    count
});

const countReducer = (state = { count: 0 }, action ) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            };
        case 'DECREMENT':
            return {
                count: state.count - action.decrementBy
            };
        case 'SET':
            return {
                count: action.count
            };
        case 'RESET':
            return {
                count: 0
            };
        default:
            return state;
    }    
};

const store = createStore(countReducer);



//automatically gets called after redux store state gets changed.
const unsubscribe = store.subscribe(() => {
    console.log('state is: ',store.getState());
});
// // to stop watching changes in redux store state 
// unsubscribe(); //this will stop the watch on state



store.dispatch(incrementCount({ incrementBy: 10 }));//object is passed with property
store.dispatch(incrementCount({ })); //object is passed without property
store.dispatch(incrementCount()); //object is not passed

store.dispatch(decrementCount());
store.dispatch(resetCount());
store.dispatch(setCount({ count: 10233 })); //forced to pass obj else error


/**
 * Old code: without destructuring the action objects
 */

// import { createStore } from 'redux';

// const store = createStore((state = { count: 0 }, action ) => {
//     switch (action.type) {
//         case 'INCREMENT':
//             const incrementBy = typeof action.incrementBy === 'number' ? action.incrementBy : 1;
//             return {
//                 count: state.count + incrementBy
//             };

//         case 'DECREMENT':
//             const decrementBy = typeof action.decrementBy === 'number' ? action.decrementBy : 1;
//             return {
//                 count: state.count - decrementBy
//             };

//         case 'SET':
//             return {
//                 count: action.count
//             };

//         case 'RESET':
//             return {
//                 count: 0
//             };
//         default:
//             return state;
//     }    
// });

// //automatically gets called after redux store state gets changed.
// const unsubscribe = store.subscribe(() => {
//     console.log('state is: ',store.getState());
// });
// // // to stop watching changes in redux store state 
// // unsubscribe(); //this will stop the watch on state

// store.dispatch({
//     type: 'INCREMENT',
//     incrementBy: 5
// });

// store.dispatch({
//     type: 'INCREMENT'
// });

// store.dispatch({
//     type: 'DECREMENT',
//     decrementBy: 20
// });

// store.dispatch({
//     type: 'RESET'
// });

// store.dispatch({
//     type: 'SET',
//     count: 1000
// });
