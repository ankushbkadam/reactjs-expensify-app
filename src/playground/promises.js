const promise = new Promise((resolve, reject) => {
    //promise can either be resolve or reject but not both 
    setTimeout(() => {
        resolve('temp message');
        //or
        //reject('Something went wrong');
    }, 5000);
});

console.log('b4');

promise.then((data) => {//if promise gets resolve call
    console.log(data);
}).catch((error) => {//if promise gets reject call
    console.log('error: ',error);
});

console.log('after');

/**
 * Output will be
 * b4
 * after
 * temp message
 * 
 * Explaination:
 * Because temp message coming from promise which will wait for 5 secs and 
 * other program statements will run asynchronously in background
 * so b4 and after will print instantly
 * after 5 secs temp message will get printed.
 */