import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import Header from '../components/Header';

export const PrivateRoute = ({
    isAuthenticated,
    component: Component, //renaming it
    ...rest //this will include all props which are not destructured (if we use ...props, it will update above destructured props)
}) => (
    <Route 
        {...rest}  
        component={(props) => (
            isAuthenticated ? (
                <div>
                    <Header />
                    <Component {...props} />
                </div>
            ) : (
                <Redirect to="/" />
            )
        )} 
    />
);

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid// here !! used for converting it to boolean value
}); 

export default connect(mapStateToProps)(PrivateRoute);