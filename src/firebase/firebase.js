import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATABASE_URL,
    projectId: process.env.FIREBASE_PROJECT_ID,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const database = firebase.database();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, googleAuthProvider, database as default };






// /**
//  * Firebase CRUD operations for reference:
//  */

// /**
//  * Create  
//  */
// database.ref().set({
//     name: "Ankush",
//     info: {
//         education: "PG",
//         address: "Pune",
//         age: 30,
//         languages: {
//             programming: [
//                 "HTML",
//                 "JAVA",
//                 "Swift",
//                 "React-JS"
//             ]
//         }
//     }
// }).then(() => { //for reference: promise.js in playground
//     console.log('data saved');
// }).catch((err) => {
//     console.log('error occoured: ',err);
// });


// //create: at specific path
// database.ref('info/languages/talking').set(["English","Hindi","Marathi"]);

// /**
//  * Read  
//  */

// //synario 1: fetch all at ones does not notify when changes made in firebase.
// // database.ref()//we can specify path in ref()
// //     .once('value')
// //     .then((snapshot) => {
// //         console.log(snapshot.val());
// //     }).catch((err) => {
// //         console.log('error reading data', err);
// //     });

// //synario 2: subscribe:fetch data from firebase whenever firebase detects change in data.
// // to unsubscribe: use off method as, database.ref().off(): this will stop observing changes.
// database.ref().on('value', (snapshot) => {//this not have then() or catch()
//     console.log(snapshot.val());
// });

// /**
//  * Update  
//  */
// database.ref('name').set('Ankush Kadam').then(()=>{console.log('name updated!')}).catch((err)=>{console.log('problem updating name')});
// database.ref('info/age').set(24);
//     //or
// database.ref('info').update({//should be object always for update()
//     percentage: {
//         PG: 74,
//         UG: 67,
//         HSC: 65,
//         SSC: 78
//     }
// });
// // database.ref().update({ //to remove/delete using update
// //     name: null
// // });
// database.ref().update({//update nested obj without specifying path in ref()
//     'info/education': "Post Graduation"
// });


// /**
//  * Delete
//  */

// //delete: (instead of remove() we can also use set(null) passing null value to set()).
// database.ref('info/languages/programming/0')
//     .remove()
//     .then(() => {
//         console.log('Data Removed!');
//     }).catch((err) => {
//         console.log('data not removed: ',err);
//     });