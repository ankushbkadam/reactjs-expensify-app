export default (expenses) => {
    return expenses
        .map((expense) => expense.amount)
        .reduce((sum, value) => sum + value, 0);
};

//here we used normal map function to get array of amount from all expenses 
//and passed array of amount to reduce function, 
//in reduce function, 0 is initial value of sum. returning sum of all 
//reference google-> javascript reduce function