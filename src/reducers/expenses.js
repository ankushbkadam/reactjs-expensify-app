
//Expense reducer:
const expensesReducerDefaultState = [];

export default (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            //return state.concat(action.expenses);
            //or : both(above,below) are exactly same and working
            return [
                ...state,
                action.expense
            ];
        case 'REMOVE_EXPENSE':
            return state.filter(({ id }) => id !== action.id );
        case 'EDIT_EXPENSE':
            return state.map((expense) => {
                if (expense.id === action.id) { //if id matches: update
                    return {
                        ...expense,             //get old values (called object spread operator)
                        ...action.updates       //override with new values
                    };
                } else {                        //if id not matches: return same data
                    return expense;
                };
            });
        case 'SET_EXPENSES':
            return action.expenses;
        default: 
            return state;
    }
};