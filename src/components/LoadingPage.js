import React from 'react';
//Loading Page
export default () => (
    <div className="loader">
        <img className="loader__image" src="/images/loader.gif" />
    </div>
);