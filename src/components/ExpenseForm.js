import React from 'react';
import moment from 'moment';
import { SingleDatePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';


//we used class component to make use of local state 
//here state is used to get the inputs from the form and when submited, submit the state
export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);

        //state can be for addExpense(empty) or editExpense(with props.expense values)
        this.state = {
            description: props.expense ? props.expense.description : '',
            note: props.expense ? props.expense.note : '',
            amount: props.expense ? props.expense.amount.toString() : '',
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false,
            error: ''
        };
    }

    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };
    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState(() => ({ note }));
    };
    onAmountChange = (e) => {
        const amount = e.target.value;
        if( !amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) { //this regular expression to accept numbers of format "<at least 1>.<at most 2>" | ex: 103384.52, 3.4, 2.45 | reference: regex101.com
            this.setState(() => ({ amount }));
        }
    };
    onDateChange = (createdAt) => {//we can use any name for param here
        if (createdAt) {
            this.setState(() => ({ createdAt }));
        }
    };
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calendarFocused: focused }));
    };
    onSubmit = (e) => {
        e.preventDefault();//to not to refresh page.
        if( !this.state.description || !this.state.amount ) {
            //error
            this.setState(() => ({ error: 'Please provide description and amount.' }));
        } else {
            //clear error if any
            this.setState(() => ({ error: '' }));

            //now submit expense obj to the parent component by prop's method
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10)/* * 100*/,       //to convert text into decimal float with base 10.(in video: converted $ into cent; 1dollar = 100cents). So removed "* 100" for rupies.
                createdAt: this.state.createdAt.valueOf(),                  //valueOf: to convert it in milliseconds
                note: this.state.note
            });
        }
    };

    render() {
        return (
            <form className="form" onSubmit={this.onSubmit}>
                { this.state.error && <p className="form__error">{this.state.error}</p> }
                <input 
                    type="text"
                    className="text-input"
                    placeholder="Description"
                    autoFocus
                    value={this.state.description}
                    onChange={this.onDescriptionChange}
                />
                <input 
                    type="text"
                    className="text-input"
                    placeholder="Amount"
                    value={this.state.amount}
                    onChange={this.onAmountChange}
                />
                <SingleDatePicker 
                    date={this.state.createdAt}
                    onDateChange={this.onDateChange}
                    focused={this.state.calendarFocused}
                    onFocusChange={this.onFocusChange}
                    numberOfMonths={1}//to show at a time
                    isOutsideRange={() => false}//this will allow user to select dates from past also.
                />
                <textarea
                    className="textarea"
                    placeholder="Add a note for your expense(Optional)"
                    value={this.state.note}
                    onChange={this.onNoteChange}
                >
                </textarea>
                <div>
                    <button className="button">Save Expense</button>
                </div>
            </form>
        );
    }
}
    